package de.itdesign.application;

import java.io.IOException;

public class Operations {

    XMLInputReader input;

    //Initializes an XML reader to access the file
    public Operations(XMLInputReader input) {

        this.input = input;
    }

    /**
     * Summates all values whose names match the filter
     * 
     * @param type   Type of the Node(Sub/Attrib)
     * @param name   Name of the Attribute
     * @param filter String to get matching values from the set
     * @return       The total of values whose names match the filter
     * @throws IOException
     */

    public String sumUp(String type, String name, String filter) throws IOException {

        double sum = 0.0;
        int i = 0;

        while (i < input.getLength()) {

            if (input.getAttribByName("name", i).matches(filter)) {

                if (type.equals("sub")) {
                    sum = sum + Double.parseDouble(input.getSubByName(name, i));
                } else {
                    sum = sum + Double.parseDouble(input.getAttribByName(name, i));
                }
                
            }

            i++;
        }

        return Double.toString(round(sum, 2));
    }

    /**
     * Summates all values whose names match the filter and returns the calculated
     * average
     * 
     * @param type   Type of the Node(Sub/Attrib)
     * @param name   Name of the Attribute
     * @param filter String to get matching values from the set
     * @return       The total of values, whose names match the filter, divided by the number of values
     * @throws IOException
     */

    public String average(String type, String name, String filter) throws IOException {

        double ave = 0.0;
        int i = 0;
        int counter = 0;

        while (i < input.getLength()) {

            if (input.getAttribByName("name", i).matches(filter)) {

                if (type.equals("sub")) {
                    ave = ave + Double.parseDouble(input.getSubByName(name, i));
                } else {
                    ave = ave + Double.parseDouble(input.getAttribByName(name, i));
                }
                counter++;
            }

            i++;
        }

        return Double.toString(round((ave / counter), 2));
    }

    /**
     * Calculates and returns the smallest attrib/sub value from the xml
     * 
     * @param type   Type of the Node(Sub/Attrib)
     * @param name   Name of the Attribute
     * @param filter String to get matching values from the set
     * @return       The smallest value within the set with matching names
     * @throws IOException
     */

    public String minimum(String type, String name, String filter) throws IOException {

        double min = 5000000.0;
        int i = 0;

        while (i < input.getLength()) {

            if (input.getAttribByName("name", i).matches(filter)) {

                if (type.equals("sub")) {

                    if (min > Double.parseDouble(input.getSubByName(name, i))) {
                        min = Double.parseDouble(input.getSubByName(name, i));
                    }

                } else {

                    if (min > Double.parseDouble(input.getAttribByName(name, i))) {
                        min = Double.parseDouble(input.getAttribByName(name, i));
                    }
                }

            }

            i++;
        }

        return Double.toString(round(min, 2));
    }

    /**
     * Determines the largest value from the filtered set of values
     * 
     * @param type   Type of the Node(Sub/Attrib)
     * @param name   Name of the Attribute
     * @param filter String to get matching values from the set
     * @return       The smallest value within the set with matching names
     * @throws IOException
     */

    public String maximum(String type, String name, String filter) throws IOException {

        double max = 0.0;
        int i = 0;

        while (i < input.getLength()) {

            if (input.getAttribByName("name", i).matches(filter)) {

                if (type.equals("sub")) {

                    if (max < Double.parseDouble(input.getSubByName(name, i))) {
                        max = Double.parseDouble(input.getSubByName(name, i));
                    }

                } else {

                    if (max < Double.parseDouble(input.getAttribByName(name, i))) {
                        max = Double.parseDouble(input.getAttribByName(name, i));
                    }
                }

            }

            i++;
        }

        return Double.toString(round(max, 2));
    }

    //Rounds the rounds the transferred value to the appropriate decimal points

    private double round(double value, int decimalPoints) {
        double d = Math.pow(10, decimalPoints);
        return Math.round(value * d) / d;
    }
}