package de.itdesign.application;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XMLInputReader {

    private Document document;
    private String tagName;
    private int length;

    /**
     * Determines the file and creates the document via a Documentbuilder, which is used to determine the tagname and number of elements
     * 
     * @param dataFile  
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    
    public XMLInputReader(String dataFile) throws SAXException, IOException, ParserConfigurationException {

        File data = new File(dataFile);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;

        documentBuilder = documentBuilderFactory.newDocumentBuilder();
        document = documentBuilder.parse(data);

        //Get the Name of the first Element Tagname
        tagName = document.getDocumentElement().getElementsByTagName("*").item(0).getNodeName();

        //Amount of Elements with Tagname
        length = document.getElementsByTagName(tagName).getLength();

    }

    /**
     * Determines the value of a child using the index and the name
     * 
     * @param name          Name of the child to be determined
     * @param id            Index of the Element in the XML
     * @return              Value of the named attribute
     * @throws Exception
     */

    public String getAttribByName(String name, int id) throws IOException {

        //return null, if Attribute does not exist or id not in bounds
        if (id >= getLength() || document.getDocumentElement().getElementsByTagName(tagName).item(id).getAttributes()
                .getNamedItem(name).getTextContent() == null ) {

            return null;

        }
               
            return document.getDocumentElement().getElementsByTagName(tagName).item(id).getAttributes()
                    .getNamedItem(name).getTextContent();

    }

    /**
     * Determines the value of an attribute using the index and the name
     * 
     * @param name          Name of the attribute to be determined
     * @param id            Index of the Element in the XML
     * @return              Value of the named attribute
     * @throws Exception
     */
    public String getSubByName(String name, int id) throws IOException {

            return document.getDocumentElement().getElementsByTagName(name).item(id).getTextContent();

    }

    public int getLength() {
        return length;
    }

}
