package de.itdesign.application;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XMLCalculator {

    public static void main(String[] args) throws UnsupportedEncodingException, IOException {
        // Don't change this part
        if (args.length == 3) {
            // Path to the data file, e.g. data/data.xml
            final String DATA_FILE = args[0];
            // Path to the data file, e.g. operations/operations.xml
            final String OPERATIONS_FILE = args[1];
            // Path to the output file
            final String OUTPUT_FILE = args[2];

            try {

                //Reader for the input data
                XMLInputReader data = new XMLInputReader(DATA_FILE);

                //Reader for the operations
                XMLInputReader operationFile = new XMLInputReader(OPERATIONS_FILE);

                //creation of the doc
                DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                Document doc = docBuilder.newDocument();
               
                Element rootElement = doc.createElement("results");
                doc.appendChild(rootElement);

                //new object for calculation operations
                Operations operations = new Operations(data);

                for (int i = 0; i < operationFile.getLength(); i++) {
                    
                    String erg;

                    switch (operationFile.getAttribByName("func", i)) {

                        case "sum":
                            erg = operations.sumUp(operationFile.getAttribByName("type", i),
                                    operationFile.getAttribByName("attrib", i),
                                    operationFile.getAttribByName("filter", i));
                            break;

                        case "average":
                            erg = operations.average(operationFile.getAttribByName("type", i),
                                    operationFile.getAttribByName("attrib", i),
                                    operationFile.getAttribByName("filter", i));
                            break;

                        case "min":
                            erg = operations.minimum(operationFile.getAttribByName("type", i),
                                    operationFile.getAttribByName("attrib", i),
                                    operationFile.getAttribByName("filter", i));
                            break;

                        case "max":
                            erg = operations.maximum(operationFile.getAttribByName("type", i),
                                    operationFile.getAttribByName("attrib", i),
                                    operationFile.getAttribByName("filter", i));
                            break;

                        default:
                            erg = null;
                            break;
                    }
                  
                    //Creation of an element with result for each operation
                    Element result = doc.createElement("result");
                    result.setAttribute("name", operationFile.getAttribByName("name", i));
                    result.appendChild(doc.createTextNode(erg));
                    rootElement.appendChild(result);

                }

               
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                // remove of the prolog and formatting
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                // initialize StreamResult with File object to save to file
                StreamResult streamResult = new StreamResult(new File(OUTPUT_FILE));
                DOMSource source = new DOMSource(doc);
                transformer.transform(source, streamResult);

            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
         

        } else {
            System.exit(1);
        }

    }

}
