## Java Challenge

Für die Challenge wurde Gradle 6.3 mit Java 13.0.2 verwendet.

Für die Ausführung wurde folgender Befehl verwendet:

  **gradle run --args='data.xml operations.xml output.xml'**